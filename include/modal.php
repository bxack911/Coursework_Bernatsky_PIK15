<div id="page_add_avatar_modal">
      <span id="page_add_avatar_modal_close">X</span>
      <div>
<?php $session_id=$_COOKIE['uid']; ?>
<div style="margin:0 auto; width:600px">
<div style="width:600px" id='page_add_avatar_modal_form'>

		<p>
			<label for="image">Image:</label>
			<input type="file" name="image" id="add_avatar_new_image" />
		</p>
		<p>
			<input type="button" id="submit_new_avatar" value="Upload!" />
		</p>

</div>
</div>
      </div>
</div>
<div id="page_add_avatar_modal_overlay"></div>

<div id="aded_modal" class="modal_window">
	<div id="aded_modal_header">
		<div>
			<a id="aded_modal_photo_but">Фото</a>
			<a id="aded_modal_video_but">Видео</a>
			<a id="aded_modal_audio_but">Аудио</a>
		</div>
		<div><a class="modal_window_close">Х</a></div>
	</div>
	<div id="aded_modal_photo">
		<div>
			<i class="fas fa-plus-square"></i>
			<input type="file" multiple="multiple" accept=".txt,image/*">
		</div>
		<a href="#" class="submit button">Загрузить файлы</a>
		<!--<div class="ajax-respond"></div>-->
	</div>
	<div id="aded_modal_video">
		<p>Для добавления видео вставте ссылку на видео с youtube ниже и придумайте заголовок для вашего видео.</p>
		<div id="aded_video_errors"></div>
		<label>URL:</label><input type="text" id="aded_modal_video_url" placeholder="URL" /><br />
		<label>Заголовок:</label><input type="text" id="aded_modal_video_title" placeholder="Заголовок" /><br />
		<button>Добавить</button>
	</div>
	<div id="aded_modal_audio">
		<p>Для добавления аудиофайла вставте ссылку на аудиофайл с soundcloud.com ниже и придумайте заголовок для вашего аудиофайла.</p>
		<label>URL:</label><input type="text" id="aded_modal_audio_url" placeholder="URL" /><br />
		<label>Заголовок:</label><input type="text" id="aded_modal_audio_title" placeholder="Заголовок" /><br />
		<button>Добавить</button>
	</div>
</div>

<script src="https://www.gstatic.com/firebasejs/3.6.4/firebase.js"></script>

<div id="video_call_modal" class="modal_window">
	<div id="aded_modal_header">
		<div>
		</div>
		<div><a class="modal_window_close modal_cideo_call_modal_close">Х</a></div>
	</div>
	<div id="video_call_modal_content">
		 <video id="yourVideo" autoplay muted></video>
		 <video id="friendsVideo" autoplay></video>
		 <br />
		 <button onclick="showFriendsFace()" type="button" class="call_button"><span class="glyphicon glyphicon-facetime-video" aria-hidden="true"></span> <i class="fas fa-phone-volume"></i></button>
		 <button onclick="hangUpCall()" type="button" class="end_call_button"><span class="glyphicon glyphicon-facetime-video" aria-hidden="true"></span> <i class="fas fa-phone"></i></button>
		 <button onclick="muteVideo()">Откл. камеру</button>
		 <button onclick="muteAudio()">Откл. звук</button>
	</div>
</div>

<!--<div id="video_call_modal" class="modal_window">
	<div id="aded_modal_header">
		<div>
		</div>
		<div><a class="modal_window_close modal_cideo_call_modal_close">Х</a></div>
	</div>
	<div id="video_call_modal_content">
		 <video id="yourVideo" autoplay muted></video>
		 <video id="friendsVideo" autoplay></video>
		 <br />
		 <button onclick="invite()" type="button" class="call_button"><span class="glyphicon glyphicon-facetime-video" aria-hidden="true"></span> <i class="fas fa-phone-volume"></i></button>
		 <button onclick="hangUpCall()" type="button" class="end_call_button"><span class="glyphicon glyphicon-facetime-video" aria-hidden="true"></span> <i class="fas fa-phone"></i></button>
		 <button>Откл. камеру</button>
		 <button>Откл. звук</button>
	</div>
</div>-->