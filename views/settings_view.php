<title>Настройки</title>
<div id="settings_cont">
	<div id="settings_menu">
		<a id="main_settings_but">Основные</a>
		<a id="notice_settings_but">Уведомление</a>
		<a id="develop_settings_but">Разработчику</a>
	</div>
	<div id="setting_content">
		<div id="main_settings">
		<h2>Основные настройки</h2>
			<div>
				<h3>Основные</h3>
				<div>
					<label>Имя:</label>
					<input type="text" placeholder="Имя" />
				</div>
				<div>
					<label>Телефон:</label>
					<input type="text" placeholder="Телефон" />
				</div>
				<div>
					<label>E-amil:</label>
					<input type="text" placeholder="E-mail" />
				</div>
			</div>
			<div id="change_pass">
				<h3>Изменить пароль</h3>
				<div>
					<label>Старый пароль:</label>
					<input type="text" placeholder="Старый пароль..." />
				</div>
				<div>
					<label>Новый пароль:</label>
					<input type="password" placeholder="Новый пароль..." />
				</div>
				<div>
					<label>Повторите ввод пароля:</label>
					<input type="password" placeholder="Новый пароль..." />
				</div>
			</div>
		</div>
		<div id="notice_settings">	
			<h2>Уведомления</h2>
		</div>
		<div id="develop_settings">
			<h2>Разработчику</h2>
			<div id="develop_settings_menu">
				<a id="develop_settings_start_but">Быстрый старт</a>
				<a id="develop_settings_auth_but">Авторизация</a>
				<a id="develop_settings_info_but">Моя информация</a>
				<a id="develop_settings_notice_but">Уведомление</a>
				<a id="develop_settings_friends_but">Друзья</a>
				<a id="develop_settings_music_but">Музыка</a>
				<a id="develop_settings_gallery_but">Галерея</a>
			</div>
			<div id="develop_settings_start">
				
				<div>
					<h3>Быстрый старт</h3>
					<div>
						<p>Наше api работает по всем привычной системе, которая включает в себя возможность получить информацию путем парсинга страниц которые возвращают методы API. Информация возвращаеться в виде json строки (в ближайшем времени будет возможность работать с XML форматом). Для получения информации вам нужно путем авторизации полуть токен, который и будет идентифицировать вас в вашей системе.</p>
						<p>На данный момент система авторизации не внедрена в API, поэтому для получения информации используеться идентификатор пользователя (и соответственно нет возможности работать с сообщениями и паролем).</p>
						<p><b>Строки запросов выглядят следующим образом:</b></p>
						<span><i>http://sn.nsh.com.ua/api/[вызываемый метод]/?параметр1=значения1&парамет2N=значения2...&параметрN=значениеN</i></span>
						<p>Методы вы можете посмотреть в меню сверху. В каждом пункте меню описаны методы, примеры работы и возможность динамически поработать с API.</p>
						<p>API может быть использовано для разработки стороннего приложения. Благодаря тому, что работа с api похожа на работу с парсером, вы можете разрабатывать приложения под разные устройства.</p>
						<p>Ниже вы можете увидеть примеры получения личной информации используя технологию ajax.</p>
						<i><pre>$.ajax({
	type: "POST",
	url: "http://sn.nsh.com.ua/api/get_user_info/?id=[Идентификатор]",
	dataType: "html",
	success: function(data)
	{
		//Переменная data будет содержать ответ в формате json
		console.log(data);
	},
	error: function(data)
	{
		alert("ERROR: "+data);
	}
});</pre></i>
						<p>Таким образом в переменной data вы получите всю базовую для работы информацию. Ниже представлен пример ответа.</p>
						<i>
	<pre>{
	"id":"3",
	"sys_email":"bxack911@gmail.com",
	"sys_telephone":"380918023071",
	"sys_link":"",
	"sys_avatar":"crop\/3-av-3-banner.jpg",
	"is_online":"1",
	"main_name":"\u041c\u0430\u043a\u0441\u0438\u043c \u0411\u0435\u0440\u043d\u0430\u0446\u044c\u043a\u0438\u0439",
	"sys_status":"active",
	"sys_priv":"admin",
	"user_friends_count":4,
	"user_friends_id":["4","11","10"],
	"user_friends_name":["\u0414\u0435\u043d\u0438\u0441 \u0428\u0430\u0442\u0438\u043b\u043e\u0432\u0438\u0447",
	"\u0438\u0432\u0430\u043d \u0438\u0432\u0430\u043d\u043e\u0432",
	"Dmitriy Akellov"],
	"user_friends_avatar":["denia.png",
						   "crop\/11-av-11-Qhc4PBQvitY.jpg",
						   "crop\/10-av-10-660a5c08f120ca4dd42c7441a53385c3.jpg"
						  ],
	"dialog_notice":{"dialog_count":0}
	}</pre>
						</i>
						<p>Для расшифровки русских символов вы можете использовать функция url_decode().</p>
					</div>
				</div>
			</div>
			<div id="develop_settings_auth">
				
				<div>
					<h3>Авторизация</h3>
					<p>На стадии разработки</p>
				</div>
			</div>
			<div id="develop_settings_info">
				
				<div>
					<h3>Личная инфа</h3>
					<div>
						<p>Используя методы этой части API, вы можете работать с такой информацией (именем, аватар, телефон, e-mail). С помощью api вы можете как получать и изменять эту информацию в вашем приложении.</p>
						<b>Методы для работы</b>
						<ul>
							<li><i>update_user_info</i></li>
							<li><i>get_user_info</i></li>
						</ul>
						<p>Методы просты, из-за того что основную информацию содержат параметры методов.</p>
						<b>update_user_info (Обновления личных данных)</b>
						<p><b><i>Поддержываемые поля:</i></b></p>
						<ul>
							<li><i>name (Имя аккаунта)</i></li>
							<li><i>telephone (Телефон. Будет использоваться как логин).</i></li>
							<li><i>email (E-mail. Будет использоваться как логин).</i></li>
						</ul>
						<b><i>Пример запроса: </i></b>
						<span><i>https://sn.nsh.com.ua/api/update_user_info/?[название поля]=[новое значение поля]</i></span>
						<p>В ответе вы получете результат OK или Error.</p>

						<b>get_user_info (Получения личных данных)</b>
						<p><b><i>Поддержываемые поля:</i></b></p>
						<ul>
							<li><i>id (Идентификатор пользователя)</i></li>
						</ul>
						<b><i>Пример запроса: </i></b>
						<span><i>http://sn.nsh.com.ua/api/get_user_info/?id=[Идентификатор пользователя]</i></span>
						<p><b><i>Привер ответа: </i></b></p>
						<i>
	<pre>{
	"id":"3",
	"sys_email":"bxack911@gmail.com",
	"sys_telephone":"380918023071",
	"sys_link":"",
	"sys_avatar":"crop\/3-av-3-banner.jpg",
	"is_online":"1",
	"main_name":"\u041c\u0430\u043a\u0441\u0438\u043c \u0411\u0435\u0440\u043d\u0430\u0446\u044c\u043a\u0438\u0439",
	"sys_status":"active",
	"sys_priv":"admin",
	"user_friends_count":4,
	"user_friends_id":["4","11","10"],
	"user_friends_name":["\u0414\u0435\u043d\u0438\u0441 \u0428\u0430\u0442\u0438\u043b\u043e\u0432\u0438\u0447",
	"\u0438\u0432\u0430\u043d \u0438\u0432\u0430\u043d\u043e\u0432",
	"Dmitriy Akellov"],
	"user_friends_avatar":["denia.png",
						   "crop\/11-av-11-Qhc4PBQvitY.jpg",
						   "crop\/10-av-10-660a5c08f120ca4dd42c7441a53385c3.jpg"
						  ],
	"dialog_notice":{"dialog_count":0}
	}</pre>
						</i>
					</div>
				</div>
			</div>
			<div id="develop_settings_notice">
				
				<div>
					<h3>Уведомления</h3>
					<p>В разработке</p>
				</div>
			</div>
			<div id="develop_settings_friends">
				
				<div>
					<h3>Друзья</h3>
				</div>
			</div>
			<div id="develop_settings_music">
				
				<div>
					<h3>Музыка</h3>
					<p>Аудиофайлы вы получаете в виде заголовка, ссылки и даты добавления. Для получения списка аудиофайлов пользователя нужно указать идентификатор пользователя.</p>
					<p><b>Пример запроса</b></p>
					<i>http://sn.nsh.com.ua/api/get_audio/?id=[Идентификатор пользователя]</i>
					<p><b>Пример ответа:</b></p>
					<i>
						<pre>
	{
	"track-0":
	{
		"id":"1",
		"title": "Fabian secon",
		"url":"https:\/\/soundcloud.com\/rapxrnb\/sets\/potlight-fabian-secon-defeated",
		"date":"2018-05-27"
	},
	"track-2":
	{
		"id":"2",
		"title": "Akellow - dark-place",
		"url":"https:\/\/soundcloud.com\/cannibal-beats\/dark-place",
		"date":"2018-05-29"
	},
	...
	"track-n":
	{
		"id":"n",
		"title": "Akellow - dark-place",
		"url":"https:\/\/soundcloud.com\/cannibal-beats\/dark-place",
		"date":"2018-05-29"
	},
	"count":2
	}
						</pre>
					</i>
				</div>
			</div>
			<div id="develop_settings_gallery">
				
				<div>
					<h3>Галерея</h3>
					<p>Для получения информации с галереи а именно фотографий и видеозаписей, вам нужно указать идентификатор пользователя, которого нужно получить галерею.</p>
					<p>Информация прийдет в виде массива json.</p>
					<p><b>Пример запроса</b></p>
					<i>http://sn.nsh.com.ua/api/get_gallery/?id=[Идентификатор пользователя]</i>
					<p><b>Пример ответа:</b></p>
					<i>
						<pre>
	{
		"gallery-0":
		{
			"url":"header.jpg",
			"id":"11",
			"date":"2018-06-02",
			"type":"photo",
			"likes_id":"163",
			"likes_is_true":1,
			"likes":1,
			"comments_count":0
		},
		"gallery-1":
		{
			"url":"https:\/\/www.youtube.com\/embed\/31j4DIpgY9U",
			"id":"12",
			"date":"2018-06-02",
			"type": "video",
			"likes_id":"163",
			"likes_is_true":1,
			"likes":1,
			"comments_count":0
		},
		"gallery_count":2
	}
						</pre>
					</i>
					<p><b>Описание ответа:</b></p>
					<ul>
						<li><i>url - ссылка на видео/фотографию.</i></li>
						<li><i>id - идентификатор. Используеться для удаления добавления коментариев.</i></li>
						<li><i>date - дата добавления.</i></li>
						<li><i>type - может быть video или photo. Используеться для разделения видео от фотографий.</i></li>
						<li><i>likes_id - идентификатор для добавления лайков.</i></li>
						<li><i>likes_is_true - есть ваш лайк на галерее или нет.</i></li>
						<li><i>likes - количество всех лайков.</i></li>
						<li><i>comments_count - количество коментариев.</i></li>
						<li><i>gallery_count - количество полученных записей.</i></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>