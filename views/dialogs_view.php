<title>Діалоги</title>
<div id="dialogs_view">
	<?php for( $i = 0; $i < $data['dialogs']["counter"]; $i++ ){ ?>
	<ul>
		<li class="dialogs_list_my_ava col-md-3">
			<img class="col-md-12" src="/img/<?php echo $data['dialogs']['mes_avatar'][$i]; ?>" alt="Image" />
		</li>
		<li class="col-md-9" id="dialog_list_name">
			<a href="/my_page/user/<?php echo $data['user_friends_id'][$i]; ?>">
				<?php echo $data['dialogs']["mes_name"][$i]; ?></a>
			 <a href="/messages/default/<?php echo $data['dialogs']['dialog_id'][$i]; ?>"><?php echo $data['dialogs']["mes_text"][$i]; ?>
			 	<?php 
			 		$id = $data['dialogs']['dialog_id'][$i];
			 		if(!empty($data["dialog_notice"]["dialog_c-$id"]))
			 		{
			 			if($data["dialog_notice"]["dialog_c-$id"]["count"] > 0)
			 			{
			 				echo "<span style='float: right;'>".$data["dialog_notice"]["dialog_c-$id"]["count"]."</span>";
			 			}
			 		} 
			 	?>
			 </a>
		</li>
		
		<li>
			<div>
				<?php echo $data['dialogs']['mes_date'][$i]; ?>
			</div>
			<div>
				<i class="fas fa-video" id='<?php echo $data['user_friends_id'][$i]; ?>'></i>
				<i class="fas fa-phone"></i>	
			</div>
		</li>
	</ul>
	<?php } ?>
</div>