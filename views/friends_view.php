<title>Друзья</title>
<div id="friends_cont">
	<div id="friends_view">
		<?php for( $i = 0; $i < $data['user_friends_count'] - 1; $i++ ){ ?>
		<ul>
			<li class="col-md-2">
				<img src="./img/<?php echo $data['user_friends_avatar'][$i]; ?>" alt="Image!" />
			</li>
			<li>
				<a href="/my_page/user/<?php echo $data['user_friends_id'][$i]; ?>" style="font-size: 18px" id="friends_view_name">
					<?php echo $data['user_friends_name'][$i]; ?>
				</a>
				<a class="new_message">Написать сообщение</a>
				<input type="hidden" value="<?php echo $data['user_friends_id'][$i]; ?>" />
				<input type="hidden" value="<?php echo $data['user_friends_name'][$i]; ?>" />
				<input type="hidden" value="<?php echo $data['user_friends_avatar'][$i]; ?>" />
			</li>
			<li>
				<i class="fas fa-video call_to_friend" id='<?php echo $data['user_friends_id'][$i]; ?>'></i>
				<input type="hidden" value="<?php echo $data['user_friends_id'][$i]; ?>" />
				<i class="fas fa-phone"></i>
			</li>
			<li class="delete_fr"><span>X</span><input type="hidden" value="<?php echo $data['user_friends_id'][$i]; ?>" /></li>
		</ul>
		<?php } ?>
	</div>
</div>