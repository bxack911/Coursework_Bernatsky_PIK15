  <title>Realtime communication with WebRTC</title>

  <title>Video Conferencing using RTCMultiConnection</title>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
  <link rel="stylesheet" href="./js/webrtc/stylesheet.css">
  <script src="./js/webrtc/menu.js"></script>

<h1>
    Video Conferencing using RTCMultiConnection
    <p class="no-mobile">
      Multi-user (many-to-many) video chat using mesh networking model.
    </p>
  </h1>

  <section class="make-center">
    <input type="text" id="room-id" value="abcdef" autocorrect=off autocapitalize=off size=20>
    <button id="open-room" onclick="openroom()">Open Room</button>
    <button id="join-room" onclick="joinroom()">Join Room</button>
    <button id="open-or-join-room" onclick="oojroom()">Auto Open Or Join Room</button>

    <div id="videos-container" style="margin: 20px 0;"></div>

    <div id="room-urls" style="text-align: center;display: none;background: #F1EDED;margin: 15px -10px;border: 1px solid rgb(189, 189, 189);border-left: 0;border-right: 0;"></div>
  </section>


<script src="./js/webrtc/RTCMultiConnection.min.js"></script>
<script src="./js/webrtc/adapter.js"></script>
<script src="./js/webrtc/socket.io.js"></script>

<link rel="stylesheet" href="/js/webrtc/getHTMLMediaElement.css">
<script src="/js/webrtc/getHTMLMediaElement.js"></script>

<script src="https://cdn.webrtc-experiment.com/common.js"></script>