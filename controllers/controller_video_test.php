<?php
class Controller_video_test extends Controller
{
	function __construct()
	{
		$this -> model = new Model_video_test();
		$this -> view = new View();
	}

	function action_default()
	{
		$data = $this -> model -> get_data();
		$this -> view -> generate("video_test_view.php", "vk.php", $data);
	}
}
?>