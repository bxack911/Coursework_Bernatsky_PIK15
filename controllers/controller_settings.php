<?php
class Controller_settings extends Controller
{
	function __construct()
	{
		$this -> model = new Model_settings();
		$this -> view = new View();
	}

	function action_default()
	{
		$data = $this -> model -> get_data();
		$this -> view -> generate("settings_view.php", "vk.php", $data);
	}
}
?>