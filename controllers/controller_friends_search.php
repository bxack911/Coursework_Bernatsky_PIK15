<?php
class Controller_friends_search extends Controller
{
	function __construct()
	{
		$this -> view = new View();
	}
	function action_default()
	{	
		$this -> model = new Model_friends_search();
		$data = $this->model->get_data();
		$this->view->generate('friends_search_view.php', 'vk.php', $data);
	}
}
?>