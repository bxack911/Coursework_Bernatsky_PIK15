<?php
class Controller_api extends Controller
{
	function __construct()
	{
		$this -> model = new Model_api();
		$this -> view = new View();
	}

	function get_params_name( $params )
	{
		$name = array();
	    $value = array();
	    $exploader = array();
	    for( $i = 0; $i < count($params); $i++ )
	    {
	        $exploader[$i] = explode('&', $params[$i]);
	    }
	    
	    for( $i = 0; $i < count($exploader); $i++ )
	    {
	        $expexp = explode('=', $exploader[$i][0]);
	        $name[$i] = $expexp[0];
	        $value[$i] = $expexp[1];
	    }

	    return $name;
	}

	function get_params_value( $params )
	{
		$name = array();
	    $value = array();
	    $exploader = array();
	    for( $i = 0; $i < count($params); $i++ )
	    {
	        $exploader[$i] = explode('&', $params[$i]);
	    }
	    
	    for( $i = 0; $i < count($exploader); $i++ )
	    {
	        $expexp = explode('=', $exploader[$i][0]);
	        $name[$i] = $expexp[0];
	        $value[$i] = $expexp[1];
	    }

	    return $value;
	}

	function action_user_reg( $params )
	{
		$data = $this->model->user_reg( $this -> get_params_name( $params ), $this -> get_params_value( $params ) );

		$this->view->generate('admin_view.php', 'api.php',$data);
	}

	function action_add_message( $params )
	{
		$data = $this->model->add_message( $this -> get_params_name( $params ), $this -> get_params_value( $params ) );

		$this->view->generate('admin_view.php', 'api.php',$data);
	}

	function action_add_friend( $params )
	{
		$data = $this->model->add_friend( $this -> get_params_name( $params ), $this -> get_params_value( $params ) );

		$this->view->generate('admin_view.php', 'api.php',$data);
	}

	function action_delete_friend( $params )
	{
		$data = $this->model->delete_friend( $this -> get_params_name( $params ), $this -> get_params_value( $params ) );

		$this->view->generate('admin_view.php', 'api.php',$data);
	}

	function action_get_user_info( $params )
	{
		$data = $this->model->get_user_info( $this -> get_params_name( $params ), $this -> get_params_value( $params ) );

		$this->view->generate('admin_view.php', 'api.php',$data);
	}

	function action_update_user_info( $params )
	{
		$data = $this->model->update_user_info( $this -> get_params_name( $params ), $this -> get_params_value( $params ) );

		$this->view->generate('admin_view.php', 'api.php',$data);
	}

	function action_get_user_dialogs( $params )
	{
		$data = $this->model->get_user_dialogs( $this -> get_params_name( $params ), $this -> get_params_value( $params ) );

		$this->view->generate('admin_view.php', 'api.php',$data);
	}

	function action_get_notice( $params )
	{
		$data = $this->model->get_notice( $this -> get_params_name( $params ), $this -> get_params_value( $params ) );

		$this->view->generate('admin_view.php', 'api.php',$data);
	}

	function action_get_audio( $params )
	{
		$data = $this->model->get_audio( $this -> get_params_name( $params ), $this -> get_params_value( $params ) );

		$this->view->generate('admin_view.php', 'api.php',$data);
	}

	function action_get_gallery( $params )
	{
		$data = $this->model->get_gallery( $this -> get_params_name( $params ), $this -> get_params_value( $params ) );

		$this->view->generate('admin_view.php', 'api.php',$data);
	}

	function action_delete_gallery( $params )
	{
		$data = $this->model->delete_gallery( $this -> get_params_name( $params ), $this -> get_params_value( $params ) );

		$this->view->generate('admin_view.php', 'api.php',$data);
	}

	function action_get_messages( $params )
	{
		$data = $this->model->get_messages( $this -> get_params_name( $params ), $this -> get_params_value( $params ) );

		$this->view->generate('admin_view.php', 'api.php',$data);
	}

	function action_add_like( $params )
	{
		$data = $this->model->add_like( $this -> get_params_name( $params ), $this -> get_params_value( $params ) );

		$this->view->generate('admin_view.php', 'api.php',$data);
	}

	function action_add_comment( $params )
	{
		$data = $this->model->add_comment( $this -> get_params_name( $params ), $this -> get_params_value( $params ) );

		$this->view->generate('admin_view.php', 'api.php',$data);
	}

	function action_gallery_modal( $params )
	{
		$data = $this->model->gallery_modal( $this -> get_params_name( $params ), $this -> get_params_value( $params ) );

		$this->view->generate('admin_view.php', 'api.php',$data);
	}

	function action_add_video( $params )
	{
		$name = array();
	    $value = array();
	    $exploader = array();
	    for( $i = 0; $i < count($params); $i++ )
	    {
	        $exploader[$i] = explode('&', $params[$i]);
	    }
	    
	    for( $i = 0; $i < count($exploader); $i++ )
	    {
	        $expexp = explode('=', $exploader[$i][0]);
	    	if( count($expexp) != 3 )
	    	{
		        $name[$i] = $expexp[0];
		        $value[$i] = $expexp[1];
	    	}
	    	else
	    	{
	    		$name[$i] = $expexp[0];
		        $value[$i] = $expexp[1]."=".$expexp[2];
	    	}
	    }

		$data = $this->model->add_video( $name, $value );

		$this->view->generate('admin_view.php', 'api.php',$data);
	}

	function action_add_audio( $params )
	{
		$data = $this->model->add_audio( $this -> get_params_name( $params ), $this -> get_params_value( $params ) );

		$this->view->generate('admin_view.php', 'api.php',$data);
	}

	function action_get_search( $params )
	{
		$data = $this->model->get_search( $this -> get_params_name( $params ), $this -> get_params_value( $params ) );

		$this->view->generate('admin_view.php', 'api.php',$data);
	}

	function action_create_dialog( $params )
	{
		$data = $this->model->create_dialog( $this -> get_params_name( $params ), $this -> get_params_value( $params ) );

		$this->view->generate('admin_view.php', 'api.php',$data);
	}

	//-------------------------------Notice
	function action_delete_dialog_notice( $params )
	{
		$data = $this->model->delete_dialog_notice( $this -> get_params_name( $params ), $this -> get_params_value( $params ) );

		$this->view->generate('admin_view.php', 'api.php',$data);
	}

	function action_search_audio( $params )
	{
		$data = $this->model->search_audio( $this -> get_params_name( $params ), $this -> get_params_value( $params ) );

		$this->view->generate('admin_view.php', 'api.php',$data);
	}
}
?>