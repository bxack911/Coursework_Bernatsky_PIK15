<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Cool net</title>
	<link rel="stylesheet" href="/css/auth.css" />
</head>
<body>
	<div id="top_header">
		<div>
			<div>
			 	<span><label>Телефон/e-mail:</label><input id="auth_login" type="text" placeholder="Логин..."></span>
			 	<span><label>Ваш пароль:</label><input id="auth_pass" type="password" placeholder="Пароль..."></span>
			 	<button id="auth_but">Войти</button>
		 	</div>
	 	</div>
   	 	<div id="logo">
   	 		<img src="http://sn.nsh.com.ua/img/logo.png" alt="Logo!">
   	 	</div>
 	</div>
 	<div id="reg">
 		<h3>Регистрация</h3>
 		<span><label>Номер телефона или e-mail:</label><input type="text" id="reg_login" placeholder="Логин..." /></span>
 		<span><label>Пароль:</label><input type="password" id="reg_pass" placeholder="Пароль..." /></span>
 		<span><label>Повторите ввод пароля:</label><input type="password" id="reg_pass_rep" placeholder="Пароль..." /></span>
 		<span><label>Имя:</label><input type="text" id="reg_name" placeholder="Имя..." /></span>
 		<button id="reg_but">Регистрация</button>
 	</div>
 <div id="content_view">
	<?php include 'views/'.$content_view; ?>
 </div>	
	<script src="/libs/jquery.js"></script>
	<script src="/js/auth.js"></script>
</body>
</html>