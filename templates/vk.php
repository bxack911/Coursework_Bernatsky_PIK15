<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" href="./libs/jquery.Jcrop.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
	<link href="https://fonts.googleapis.com/css?family=Shadows+Into+Light" rel="stylesheet">

	<link rel="stylesheet" href="./libs/player/css/sc-player-minimal.css" type="text/css" />

	<script src="https://www.gstatic.com/firebasejs/3.6.4/firebase.js"></script>

	<link rel="stylesheet" href="/css/vk.css">
	<link rel="stylesheet" href="/css/modal.css">
</head>
<body>
<div id="overlay"></div>
<div id="new_message_modal">
	<img src="" alt="" />
	<p></p>
	<textarea cols="30" rows="10" placeholder="Вводите текст..."></textarea>
	<button>Отправить</button>
	<input type="hidden" id="fr_id" value="" />
</div>
<?php include "include/modal.php"; ?>
   <div id="header">
   		<div id="avatar">
   			<img src="http://sn.nsh.com.ua/img/<?php echo ($data['sys_avatar'] == '') ? 'no_photo.jpg' : $data['sys_avatar']; ?>" alt="<?php echo $data['sys_avatar']; ?>">
   			<div>
	   			<p><?php echo $data['main_name']; ?></p>
		    	<p>
		            <?php echo ($data['is_online'] == 1 ? "Онлайн" : "Оффлайн"); ?>   
		        </p>
	        </div>
	        <?php if($_COOKIE["priv"] == "admin"){ ?>
	        <div>
	        	<?php if($data["sys_status"] == "active" && $data["sys_priv"] != "admin"){ ?>
	        		<p style='color: red; cursor: pointer;' class="admin_act" act="banned">Забанить</p>
	        		<input type="hidden" value="<?php echo $data['id']; ?>" />
	        	<?php }else if($data["sys_priv"] != "admin"){ ?>
	        		<p style='color: green; cursor: pointer;' class="admin_act" act="activate">Разблочить</p>
	        		<input type="hidden" value="<?php echo $data['id']; ?>" />
	        	<?php } ?>
	        </div>
	        <?php } ?>
   		</div>
   	 	<div id="search">
   	 		<?php if($data["sys_status"] == "active"){ ?>
   	 			<input type="text" placeholder="Поиск...">
   	 			<div></div>
   	 		<?php } ?>
   	 	</div>
   	 	<div id="logo"><img src="http://sn.nsh.com.ua/img/logo.png" alt="Logo!"></div>
   	 	<!--<ul>
   	 		<li><a href="/friends_search">Пошук</a></li>
   	 		<li><a>Карти</a></li>
   	 		<li><a>Підтримка</a></li>
   	 		<li id="my_page_logout"><a>Вийти</a></li>
   	 	</ul>-->
   </div>
   <?php if($_COOKIE["status"] == "active"){ ?>
		 <div id="main_menu" class="col-md-2">
		 	<div id="main_manu_links">
		 		<a href="/">Моя страница</a>
		 		<a href="/friends">Друзья</a>
		 		<a href="/dialogs">
		 			Диалоги
		 			<span><?php if($data["dialog_notice"]["dialog_count"] > 0){ ?>
		 				<?php echo $data["dialog_notice"]["dialog_count"]; ?>
		 			<?php } ?>
		 			</span>
		 		</a>
		 		<a href="/audio">Аудио</a>
		 		<a href="/settings">Настройки</a>
	 		</div>
	 		<div id="main_menu_icons">
	 			<i class="fas fa-video"></i>
	 			<i class="fas fa-plus" id="aded_action"></i>
	 			<p id="my_page_logout">Выход</p>
	 		</div>
		 </div>
	<?php } if($data["sys_status"] == "active"){ ?>
		 <div class="col-md-9" id="content">
			<?php include 'views/'.$content_view; ?>
		 </div>		
	<?php }else{ ?>
		<div id="banned_act">
			<?php if($_COOKIE['uid'] == $data["id"]){ ?>
			<div id="main_menu" class="col-md-2">
	 		<div id="main_menu_icons">
	 			<p id="my_page_logout">Выход</p>
	 		</div>
		 </div>
		 <?php } ?>

			<span><?php echo $data["main_name"]; ?> заблокирован администрацией!</span> Ожидайте разблокировки или свяжитесь с администрацией.<br />
		</div>
	<?php } ?>
		 <input type="hidden" id="user_id" value="<?php echo $_COOKIE['uid']; ?>" />
	   
	<script src="/libs/jquery.js"></script>
	<script src="/libs/jquery.Jcrop.js"></script>
	
	<?php if($_COOKIE["priv"] == "admin"){ ?>
		<script src="/js/admin.js"></script>
	<?php } ?>
	<script src="/js/basic.js"></script>
	<script src="/js/modal.js"></script>
	
    <script type="text/javascript" src="./libs/player/js/soundcloud.player.api.js"></script>
    <script type="text/javascript" src="./libs/player/js/sc-player.js"></script>
</body>
</html>