//Відео-чат
var config = 
{
    apiKey: "AIzaSyA7m139DR91sbHinG8_QzVE8WgO-yAD7Qc",
    authDomain: "snnsh-5e384.firebaseapp.com",
    databaseURL: "https://snnsh-5e384.firebaseio.com",
    projectId: "snnsh-5e384",
    storageBucket: "",
    messagingSenderId: "493174809361"
};
firebase.initializeApp(config);

var yourVideo = document.getElementById("yourVideo");
var friendsVideo = document.getElementById("friendsVideo");
var myUsername;
var targetUser;
var servers = {'iceServers': [{'urls': 'stun:stun.services.mozilla.com'}, {'urls': 'stun:stun.l.google.com:19302'}, {'urls': 'turn:numb.viagenie.ca','credential': 'beaver','username': 'webrtc.websitebeaver@gmail.com'}]};
var pc = new RTCPeerConnection(servers);
var database = firebase.database().ref();
var userTarget;

//function connect() 
//{
    database = firebase.database().ref();
    yourVideo = document.getElementById("yourVideo");
    friendsVideo = document.getElementById("friendsVideo");
    myUsername = $("#user_global_name").val();
    userTarget = targetUser;
    //Create an account on Viagenie (http://numb.viagenie.ca/), and replace {'urls': 'turn:numb.viagenie.ca','credential': 'websitebeaver','username': 'websitebeaver@email.com'} with the information from your account
    servers = {'iceServers': [{'urls': 'stun:stun.services.mozilla.com'}, {'urls': 'stun:stun.l.google.com:19302'}, {'urls': 'turn:numb.viagenie.ca','credential': 'beaver','username': 'webrtc.websitebeaver@gmail.com'}]};
    pc = new RTCPeerConnection(servers);
    pc.onicecandidate = (event => event.candidate?sendToServer({type: "new-ice-candidate", target: userTarget, ice: event.candidate}):console.log("Sent All Ice") );
    pc.onaddstream = (event => friendsVideo.srcObject = event.stream);
database.on('child_added', readMessage);
    function readMessage(evt) {
        var jssPARSE = JSON.parse(evt.A.B);
        console.dir(jssPARSE);
        //var type = jssPARSE.type;
        //var sender = jssPARSE.name;
        //var target = jssPARSE.target;
        //var ice = jssPARSE.ice;
        //var sdp = jssPARSE.sdp;
        console.log(jssPARSE.type);
        console.log(jssPARSE.name);
        console.log(jssPARSE.target);
        //console.log(jssPARSE.ice);
        //console.log(jssPARSE.sdp);
        if (jssPARSE.target == myUsername && jssPARSE.type == "call") 
        {
            //alert("Vam zvonit " + jssPARSE.name);
            //console.log("Vam zvonit " + jssPARSE.name);
            document.getElementById(sounds.rington).play();
            alertMsg(jssPARSE);
        }
        if (jssPARSE.name != myUsername) {
            if (jssPARSE.ice != undefined){
                pc.addIceCandidate(new RTCIceCandidate(jssPARSE.ice));
            }
            else if (jssPARSE.type == "video-offer"){
                pc.setRemoteDescription(new RTCSessionDescription(jssPARSE.sdp))
                  .then(() => pc.createAnswer())
                  .then(answer => pc.setLocalDescription(answer))
                  .then(() => sendToServer({name: myUsername, type: "video-answer", target: userTarget, sdp: pc.localDescription}));
            }
            else if (jssPARSE.type == "video-answer"){
                showFriendsFace();
                document.getElementById(sounds.calling).pause();
                pc.setRemoteDescription(new RTCSessionDescription(jssPARSE.sdp));
            }
            else if (jssPARSE.type == "hang-up"){
            	closeVideoCall();
            }
            else if (jssPARSE.type == "call-To-Off"){
            	alert(jssPARSE.name + " отклонил ваш вызов");
            }
            else if (jssPARSE.type == "callon"){
            	showFriendsFace();
            }
   		}
};
//}


function alertMsg(jssPARSE) 
{
	var answer = confirm("Вам звонит " + jssPARSE.name + "<br />" + "Желаете ответить на звонок?");
  if (answer) 
	{
    document.getElementById(sounds.rington).pause();
		$("#overlay").css("display","block");
		$("#video_call_modal").css("display","block");
		showFace();
	}
	else
	{
    document.getElementById(sounds.rington).pause();
    document.getElementById(sounds.end).play();
	sendToServer({name: myUsername, type: "call-To-Off", target: userTarget});
	}
}


function sendToServer(massage) 
{
    var massageJSON = JSON.stringify(massage);
    console.log("Sending '" + massage.type + "' message: " + massageJSON);
    var msg = database.push(massageJSON);
    msg.remove();
}

var sounds = {
            'calling': 'callingSignal',
            'end': 'endCallSignal',
            'rington': 'ringtoneSignal'
        };

//database.on('child_added', readMessage);
function showFace() {
	userTarget = targetUser;
	sendToServer({type: "callon"});
  	navigator.mediaDevices.getUserMedia({audio:true, video:true})
    .then(stream => yourVideo.srcObject = stream)
    .then(stream => pc.addStream(stream));
}
function showMyFace() {
	userTarget = targetUser;
  	showFriendsFace();
  	document.getElementById(sounds.calling).play();
	sendToServer({name: myUsername, type: "call", target: userTarget});
  	navigator.mediaDevices.getUserMedia({audio:true, video:true})
    .then(stream => yourVideo.srcObject = stream)
    .then(stream => pc.addStream(stream));
}

function showFriendsFace() {
  pc.createOffer()
    .then(offer => pc.setLocalDescription(offer) )
    .then(() => sendToServer({name: myUsername, type: "video-offer", target: userTarget, sdp: pc.localDescription}));
}

function hangUpCall() 
{
  closeVideoCall();
  sendToServer({
    name: myUsername,
    type: "hang-up",
    target: userTarget
  });
}

function closeVideoCall() 
{
  document.getElementById(sounds.calling).pause();
  document.getElementById(sounds.end).play();
  var remoteVideo = document.getElementById("friendsVideo");
  var localVideo = document.getElementById("yourVideo");

  console.log("Closing the call");

  if (pc) 
  {
  	console.log("--> Closing the peer connection");

    pc.onaddstream = null;
    pc.ontrack = null;
    pc.onremovestream = null;
    pc.onnicecandidate = null;
    pc.oniceconnectionstatechange = null;
    pc.onsignalingstatechange = null;
    pc.onicegatheringstatechange = null;
    pc.onnotificationneeded = null;

    if (remoteVideo.srcObject) 
    {
      remoteVideo.srcObject.getTracks().forEach(track => track.stop());
    }

    if (localVideo.srcObject) 
    {
      localVideo.srcObject.getTracks().forEach(track => track.stop());
    }

    remoteVideo.src = null;
    localVideo.src = null;

    pc.close();
    pc = null;

    $(".modal_window").css("display","none");
    $("#overlay").css("display","none");
  }
}


//Створення ajax
var host = "https://sn.nsh.com.ua";
function create_ajax(url,success)
{
	$.ajax({
		type: "POST",
		url: url,
		dataType: "html",
		asynch: true,
		cache: false,
		success: function(data)
		{
			success(data);
		},
		error: function(data)
		{
			alert("ERROR: "+data);
		}	
	});
}

$(document).ready(function(){
	$("#messages_row").scrollTop($("#messages_row").prop('scrollHeight'));

	$("#my_page_logout").click(function(){
		function success(data)
		{
			window.location.reload();
		}
		create_ajax("/progs/logout.php",success);
	});

	$("#page_add_wall_submit").click(function(){
		var text = $("#page_add_wall_textarea").val();

		function success(data)
		{
			if(data == "OK")
				    window.location.reload();
		}
		create_ajax("/progs/wall_add.php",success);
	});

		$("#page_add_wall_textarea").focus(function(){
		$(this).attr("rows","5");
		$("#page_add_wall_hide").css("display", "block");
	});

	$("#page_add_wall_textarea").focusout(function(){
		if($("#page_add_wall_submit").click())
			$(this).attr("rows","1");
		$("#page_add_wall_hide").css("display", "none");
	});

	$("#messages_new_message_textarea").keydown(function(eventObject){
		if( $("#messages_new_message_textarea").val() == "" )
		{
			$("#messages_new_message_button_li").css("display", "none");
		}
		else
		{
			$("#messages_new_message_button_li").css("display", "block");
		}
	});

	$("#messages_new_message_textarea").keyup(function(e){
		if(e.ctrlKey && e.keyCode == 13)
		{
			var text = $("#messages_new_message_textarea").val();
			var id = $("#messages_new_message_button_li").attr("idd");

			function success(data)
			{
				window.location.reload();
			}
			create_ajax(host+"/api/add_message/?"+id+"="+text,success);
		}
	});

	$("#messages_new_message_button_li").click(function(){

		var text = $("#messages_new_message_textarea").val();
		var id = $("#messages_new_message_button_li").attr("idd");

		if(text != "")
		{
			$("#messages_new_message_textarea").css("border","1px solid #ccc");
			function success(data)
			{
				window.location.reload();
			}
			create_ajax(host+"/api/add_message/?"+id+"="+text,success);
		}
		else
		{
			$("#messages_new_message_textarea").css("border","1px solid #E82020");
		}
	});

	$(".friends_search_add_button").click(function(){
		var id = $(this).attr('id');
		function success(data)
		{
			window.location.reload();
		}
		create_ajax(host+"/api/add_friend/?*="+id,success);
		
	});

	$(".page_wall_drop").click(function(){
		function success(data)
		{
			window.location.reload();
		}
		create_ajax(host+"/api/drop_wall/?id="+$(this).attr("id"),success);
	});

	$(".smile").click(function(){
		var smile = $(this).attr('alt');
		var text = $.trim($("#messages_new_message_textarea").val());
		$("#messages_new_message_textarea").focus().val(text+' '+smile+' ');
	});

	$("#my_page_gallery_list_bottom span").click(function(){
		var gal_id = $(this).attr("gal_id");
		var cok = $(this).attr("cok");
		var is_true = $(this).attr("is_true");
		var count = $(this).attr("count");
		var node = $(this);

		//alert("http://sn.nsh.com.ua/api/add_like/?"+gal_id+"="+cok+"&"+is_photo+"="+is_true);
		function success(data)
		{
			if(data != "")
			{
				node.attr("gal",data);
			}
			else if(data == 0)
			{
				node.attr("gal",0);
			}
			if(is_true == 0)
			{
				var cc = parseInt(count)+1;
				node.html("<i class='fas fa-heart'></i> "+cc);
				node.attr("is_true",1);
				node.css("color","#7069FF");
				node.attr("count",cc);
			}
			else
			{
				var cc = parseInt(count)-1;
				node.html("<i class='fas fa-heart'></i> "+cc);
				node.attr("is_true",0);
				node.css("color","#545454");
				node.attr("count",cc);
			}
		}
		create_ajax(host+"/api/add_like/?"+gal_id+"="+cok+"&active="+is_true,success);
	});

	$("#aded_modal_video > button").click(function(){
		var cok = $("#user_id").val();
		var title = $("#aded_modal_video_title").val();
		var url = $("#aded_modal_video_url").val();

		function success(data)
		{
			if(data == "parse_error")
			{
				$("#aded_video_errors").css("color","red");
				$("#aded_video_errors").html("Ошибка при добавлении. Возможно вы пытаетесь добавить видеозапись не с сайта youtube! Попробуйте еще раз");
			}
			else
			{
				$("#aded_video_errors").css("color","green");
				$("#aded_video_errors").html("Добавлено");
				window.location.reload();
			}
		}
		create_ajax(host+"/api/add_video/?"+cok+"="+title+"&video="+url,success);
	});

	$("#aded_modal_audio > button").click(function(){
		var cok = $("#user_id").val();
		var title = $("#aded_modal_audio_title").val();
		var url = $("#aded_modal_audio_url").val();

		function success(data)
		{
			window.location.reload();
		}
		create_ajax(host+"/api/add_audio/?"+cok+"="+title+"&url="+url,success);
	});


	$("#search > input").keyup(function(){
		$("#overlay").css("display","block");
		$("#header > #search").css("z-index","3");
		$("#header > #search > input").css("z-index","3");
		$("#search > div").css("z-index","3");

		var cok = $("#user_id").val();
		var val = $(this).val();
		if(val != "")
		{
			function success(data)
			{
				$("#search > div").html(data);
			}
			create_ajax(host+"/api/get_search/?"+cok+"="+val,success);
		}
		else
		{
			$("#overlay").css("display","none");
			$("#header > #search").css("z-index","0");
			$("#header > #search > input").css("z-index","0");
			$("#search > div").css("z-index","0");

			$(this).val("");
			$("#search > div").html("");
		}
	});

	$("#overlay").click(function(){
		$("#new_message_modal").css("display","none");
		$("#overlay").css("display","none");
		$("#header > #search").css("z-index","0");
		$("#header > #search > input").css("z-index","0");
		$("#search > div").css("z-index","0");

		$("#search > input").val("");
		$("#search > div").html("");
	});

	$("body").on("click","#sow_add_fr_tr",function(){
		var cok = $("#user_id").val();
		var fr =$(this).next().val();
		var node = $(this);
		function success(data)
		{
			node.attr("id","sow_mes_modal");
			node.html("Написать сообщение");
			$("#search > div").html(data);
		}
		create_ajax(host+"/api/add_friend/?"+cok+"="+fr,success);
	});

	$("body").on("click","#photo_modal button",function(){
		var cok = $("#user_id").val();
		var gal_id = $(this).next().val();
		var text = $(this).prev().val();
		var node = $(this);
		function success(data)
		{
			node.prev().prev().prepend(data);
		}
		create_ajax(host+"/api/add_comment/?"+cok+"="+gal_id+"&text="+text,success);
	});

	$("body").on("click", ".new_message", function(){
		$("#overlay").css("display","block");
		var id = $(this).next().val();
		var name = $(this).next().next().val();
		var ava = $(this).next().next().next().val();
		$("#new_message_modal").css("display","block");
		$("#new_message_modal > img").attr("src", "./img/"+ava);
		$("#new_message_modal > p").html(name);
		$("#new_message_modal > #fr_id").val(id);

		$("#header > #search").css("z-index","0");
		$("#header > #search > input").css("z-index","0");
		$("#search > div").css("z-index","0");

		$("#search > input").val("");
		$("#search > div").html("");
	});

	/*$("body").on("click","#friends_view > ul > li:nth-child(2) > a:nth-child(2)",function(){
		var cok = $("#user_id").val();
		var us_id = $(this).next().val();

		function success(data)
		{
			window.location.href="http://sn.nsh.com.ua/messages/default/"+data;
		}
		create_ajax("http://sn.nsh.com.ua/api/create_dialog/?"+cok+"="+us_id,success);
	});*/

	$("#new_message_modal > button").click(function(){
		var cok = $("#user_id").val();
		var id = $(this).next().val();
		var text = $(this).prev().val();

		if(text != "")
		{
			$(this).prev().css("border","1px solid #ccc");
			function success(data)
			{
				window.location.href="http://sn.nsh.com.ua/messages/default/"+data;
			}
			create_ajax(host+"/api/create_dialog/?"+cok+"="+id+"&text="+text,success);
		}
		else
		{
			$(this).prev().css("border","1px solid #E82020");
		}
	});

	$(".delete_fr").click(function(){
		var cok = $("#user_id").val();
		var id = $(this).children("input").val();
		alert(id);

		function success(data)
		{
			window.location.reload();
		}
		create_ajax(host+"/api/delete_friend/?"+cok+"="+id,success);
	});

	$("#my_page_gallery_list_top > p:nth-child(2)").click(function(){
		var gal_id = $(this).next().val();
		var cok = $("#user_id").val();

		function success(data)
		{
			window.location.reload();
		}
		create_ajax(host+"/api/delete_gallery/?id="+gal_id,success);
	});

	$("#music_page > input").keyup(function(){
		var node = $(this);
		var query = $(this).val();

		if(query != "")
		{
			function success(data)
			{
				if(data == "")
				{
					node.next().css("display","none");
					node.next().html("");
				}
				else
				{
					node.next().css("display","block");
					node.next().html(data);
				}
			}
			create_ajax(host+"/api/search_audio/?query="+query,success);
		}
		else
		{
			node.next().css("display","none");
			node.next().html("");
		}
	});

	$(".call_to_friend").click(function(){
		$("#overlay").css("display","block");
		$("#video_call_modal").css("display","block");
		targetUsername = $(this).next().val();
		targetUser = $(this).next().next().val();
		showMyFace();
		//connect();
	});

	$(".end_call_button").click(function(){
		$(".modal_window").css("display","none");
		$("#overlay").css("display","none");
		//ex();
	});
//---------------------------Update avatar (uploaded and cropping)

	$(document).on("change", "#add_avatar_new_image", function(){

		var data = new FormData();
		$.each($('#add_avatar_new_image')[0].files, function(i, file) {
    	data.append('file-'+i, file);
		});

		$.ajax({
    		url: '/progs/upload_new_avatar.php',
    		data: data,
    		cache: false,
    		contentType: false,
    		processData: false,
    		type: 'POST',
    		success: function(data){
        		var dat = data.split('=');
        		var filenamee = dat[1].split('&');
        		var filename = filenamee[0];
        		var height = dat[2];
        		/*alert(filename);
        		alert(height);*/
        		$.ajax({
					type: "POST",
					url: "/progs/cropping.php",
					data: "filename="+filename+"&height="+height,
					dataType: "html",
					asynch: false,
					cache: false,
					success: function(data)
					{
						$("#page_add_avatar_modal_form").html(data);

						$("#page_add_avatar_modal_avatar_submit_crop_button").click(function(){
							
							var x = $("#page_add_avatar_modal_avatar_submit_crop_x").val();
							var y = $("#page_add_avatar_modal_avatar_submit_crop_y").val();
							var w = $("#page_add_avatar_modal_avatar_submit_crop_w").val();
							var h = $("#page_add_avatar_modal_avatar_submit_crop_h").val();
							var file = $("#page_add_avatar_modal_avatar_filename").val();

							$.ajax({
								type: "POST",
								url: "/progs/create_crop.php",
								data: "filename="+file+"&x="+x+"&w="+w+"&h="+h,
								dataType: "html",
								asynch: false,
								cache: false,
								success: function(data)
								{
									if(data == "OK")
									{
										window.location.href = "/";
										window.location.reload;
									}
								}
							});
						});
					}
		});
    		}
		});
	});
});