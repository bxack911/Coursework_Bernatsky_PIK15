<?php
class Model_api extends Model
{
	public function user_reg( $params_name, $params_value )
	{
		$us = DB::getInstance() -> query("SELECT id,sys_status,sys_priv FROM us_main WHERE sys_email = '".$params_name[0]."' OR sys_telephone = '".$params_name[0]."'");

		if($us->count() == 0)
		{
			if($params_value[0] == "tel")
			{
				DB::getInstance() -> no_returns("INSERT INTO us_main(sys_email,sys_telephone,pass,sys_link,sys_avatar,is_online,main_name,rel_friends)VALUES(
						'',
						'".$params_name[0]."',
						'fds8Fg".base64_encode($params_name[1])."cvxcRt5',
						'',
						'',
						0,
						'".htmlspecialchars(urldecode($params_value[1]))."',
						''
					)");
			}
			else
			{
				DB::getInstance() -> no_returns("INSERT INTO us_main(sys_email,sys_telephone,pass,sys_link,sys_avatar,is_online,main_name,rel_friends)VALUES(
						'".$params_name[0]."',
						'',
						'fds8Fg".base64_encode($params_name[1])."cvxcRt5',
						'',
						'',
						0,
						'".htmlspecialchars(urldecode($params_value[1]))."',
						''
					)");
			}
		}
		else
		{
			echo "User exists!";
		}
	}

	public function add_message( $params_name, $params_value )
	{
		date_default_timezone_set("Europe/Kiev");
		DB::getInstance() -> no_returns("INSERT INTO us_messages (from_id, dialog_id, text, date) VALUES(
				".$_COOKIE['uid'].",
				".$params_name[0].",
				'".base64_encode(htmlspecialchars(urldecode($params_value[0])))."',
				'".date('Y-m-d')."'
			)");
	}

	public function add_like( $params_name, $params_value )
	{
		$gal_id = $params_name[0];
		$user_id = $params_value[0];
		$is_true = $params_value[1];


			if($is_true == 1)
			{
				DB::getInstance() -> no_returns("DELETE FROM us_likes WHERE gal_id = ".$gal_id." AND user_id = ".$user_id);
				return 0;
			}
			else
			{
				DB::getInstance() -> no_returns("INSERT INTO us_likes(gal_id,user_id)
				VALUES(
					".$gal_id.",
					".$user_id."
				)");
				//$qqq = DB::getInstance() -> query("SELECT id FROM us_likes WHERE is_photo = 1");
				return "OK";
				//return $qqq -> results()[$qqq->count()-1] -> id;
			}
	}

	public function add_comment( $params_name, $params_value )
	{
		$gal_id = $params_value[0];
		$user_id = $params_name[0];
		$text = base64_encode(htmlspecialchars(urldecode($params_value[1])));

		DB::getInstance() -> no_returns("INSERT INTO us_comments(gal_id,user_id,text)VALUES(
					".$gal_id.",
					".$user_id.",
					'".$text."'
				)");

		$user = DB::getInstance() -> query("SELECT main_name,sys_avatar FROM us_main WHERE id = ".$user_id);
		$ava = $user->results()[0] -> sys_avatar;
		$name = $user->results()[0] -> main_name;
		$txt = base64_decode($text);

		$html = "<div>
                    <div>
                        <img src='http://sn.nsh.com.ua/img/".$ava."' alt='' />
                        <p>".$name."</p>
                    </div>
                    <p>".$txt."</p>
                </div>";

		return $html;
	}

	public function add_friend( $params_name, $params_value )
	{
		$now_fr = DB::getInstance() -> query("SELECT rel_friends FROM us_main WHERE id = ".$params_name[0]." LIMIT 1");

		$fr = $now_fr -> results()[0] -> rel_friends.$params_value[0].";";

		DB::getInstance() -> no_returns("UPDATE us_main SET rel_friends = '".$fr."' WHERE id = ".$params_name[0]);
	}

	public function delete_friend( $params_name, $params_value )
	{
		$now_fr = DB::getInstance() -> query("SELECT rel_friends FROM us_main WHERE id = ".$params_name[0]." LIMIT 1");

		$fr = str_replace($params_value[0].";", "",$now_fr->results()[0] -> rel_friends);
		DB::getInstance() -> no_returns("UPDATE us_main SET rel_friends='".$fr."'  WHERE id = ".$params_name[0]);
	}

	public function add_video( $params_name, $params_value )
	{
		date_default_timezone_set("Europe/Kiev");

		$link = $params_value[1];
		$url_link = explode("v=",$params_value[1]);
		$url_tst = explode("youtube",$params_value[1]);

		if(count($url_tst) > 1)
		{
			if(count($url_link) > 1)
			{
				$link = "https://www.youtube.com/embed/".$url_link[1];
			}
			else
			{
				$link = $params_value[1];
			}

			DB::getInstance() -> no_returns("INSERT INTO us_gallery(user_id,url,date,type)VALUES(
						".$params_name[0].",
						'".$link."',
						'".date('Y-m-d')."',
						'video'
					)");
		}
		else
		{
			return "parse_error";
		}
	}

	public function add_audio( $params_name, $params_value )
	{
		date_default_timezone_set("Europe/Kiev");


		DB::getInstance() -> no_returns("INSERT INTO us_audio(user_id,title,url,date)VALUES(
					".$params_name[0].",
					'".htmlspecialchars(urldecode($params_value[0]))."',
					'".$params_value[1]."',
					'".date('Y-m-d')."'
				)");
	}

	public function get_user_info( $params_name, $params_value )
	{
		require_once './include/api/get_user_info.php';
		echo json_encode($data);
	}

	public function update_user_info( $params_name, $params_value )
	{
		switch($params_value[0])
		{
			case 'name':
				DB::getInstance() -> no_returns("UPDATE us_main SET main_name = '".urldecode($params_value[0])."' WHERE id = ".$params_name[0]);
			break;
			case 'tel':
				DB::getInstance() -> no_returns("UPDATE us_main SET sys_telephone = '".urldecode($params_value[0])."' WHERE id = ".$params_name[0]);
			break;
			case 'mail':
				DB::getInstance() -> no_returns("UPDATE us_main SET sys_email = '".urldecode($params_value[0])."' WHERE id = ".$params_name[0]);
			break;
			case 'pass':
				$que = DB::getInstance() -> no_returns("SELECT pass FROM us_main WHERE id = ".$params_name[0]);
				if(!empty($que -> results()[0] -> pass))
				{
					if('fds8Fg'.base64_encode($params_value[1]).'cvxcRt5' == $que -> results()[0] -> pass)
					{
						if($params_name[1] == $params_value[1])
						{
							DB::getInstance() -> no_returns("UPDATE us_main SET pass = '".'fds8Fg'.base64_encode($params_value[1]).'cvxcRt5'."' WHERE id = ".$params_name[0]);
						}
						else
						{
							return "reppass_err";
						}
					}
					else
					{
						return "pass_err";
					}
				}
				else
				{
					return "pass_err";
				}
			break;
		}
	}

	public function get_audio( $params_name, $params_value )
	{
		$data = array();

		$audio = DB::getInstance() -> query("SELECT * FROM us_audio WHERE user_id = ".$params_value[0]);
		$audio_count = $audio -> count();

		for($i = 0; $i < $audio_count; $i++)
		{
			$data["track-$i"]["id"] = $audio -> results()[$i] -> id;
			$data["track-$i"]["title"] = $audio -> results()[$i] -> title;
			$data["track-$i"]["url"] = $audio -> results()[$i] -> url;
			$data["track-$i"]["date"] = $audio -> results()[$i] -> date;
		}

		$data["count"] = $audio_count;

		return json_encode($data);
	}

	public function get_gallery( $params_name, $params_value )
	{
		$data = array();

		$photo = DB::getInstance() -> query("
			SELECT us_gallery.*
			FROM us_gallery
			WHERE us_gallery.user_id=".$params_value[0]." ORDER BY us_gallery.id DESC");
		$photo_counter = $photo -> count();
		$gallery_count = 0;

		$comments_count = 0;
		$likes_count = $photo_counter;
		$likes_countt = $photo_counter;
		$db_counter = $photo_counter;
		for($i = 0; $i < $photo_counter; $i++)
		{
			$data["gallery-$i"]['url'] = $photo -> results()[$i] -> url;
			$data["gallery-$i"]['id'] = $photo -> results()[$i] -> id;
			$data["gallery-$i"]['date'] = $photo -> results()[$i] -> date;
			$data["gallery-$i"]['type'] = $photo -> results()[$i] -> type;
			$data["gallery-$i"]['user_id'] = $photo -> results()[$i] -> user_id;
			$data["gallery-$i"]['from_id'] = $photo -> results()[$i] -> from_id;


			$likes = DB::getInstance() -> query("SELECT * FROM us_likes WHERE gal_id = ".$photo -> results()[$i] -> id." AND user_id = ".$params_value[0]);
			$likes_countt = $likes -> count();
			$likes_counter = 0;
			$is_true = false;
			if($likes_countt > 0)
			{
				for($j = $db_counter; $j < $db_counter+$likes_countt; $j++)
				{
					if($is_true == false)
					{
						$data["gallery-$i"]['likes_id'] = $photo -> results()[$j] -> id;
						$data["gallery-$i"]['likes_is_true'] = 1;
						$is_true=true;
					}
					$likes_counter++;
				}
			}
			$db_counter+=$likes_counter;
			$data["gallery-$i"]['likes'] = $likes_counter;


			$comments = DB::getInstance() -> query("SELECT us_comments.text,us_comments.user_id,
														   us_main.main_name AS us_main,us_main.sys_avatar AS us_avatar
						FROM us_comments
						LEFT JOIN us_main ON us_comments.user_id=us_main.id
						WHERE gal_id = ".$photo -> results()[$i] -> id);
			$comments_count = $comments->count();
			$comments_counter = 0;

			for($j = $db_counter; $j < $db_counter+$comments_count; $j++)
			{
				$data["gallery-$i"]["comments-$comments_counter"]["text"] = base64_decode($comments -> results()[$j] -> text);
				$data["gallery-$i"]["comments-$comments_counter"]["user"] = $comments -> results()[$j] -> user_id;

				$data["gallery-$i"]["comments-$comments_counter"]["us_name"] = $comments -> results()[$j] -> us_main;
				$data["gallery-$i"]["comments-$comments_counter"]["us_avatar"] = $comments -> results()[$j] -> us_avatar;
				$comments_counter++;
			}


			$db_counter+=$comments_count;
			$data["gallery-$i"]['comments_count'] = $comments_count;

			$gallery_count++;
		}

		$data['gallery_count'] = $gallery_count;

		echo json_encode($data);
	}

	public function delete_gallery( $params_name, $params_value )
	{
		DB::getInstance() -> no_returns("DELETE FROM us_gallery WHERE id = ".$params_value[0]);
		DB::getInstance() -> no_returns("DELETE FROM us_likes WHERE gal_id = ".$params_value[0]);
		DB::getInstance() -> no_returns("DELETE FROM us_comments WHERE gal_id = ".$params_value[0]);
	}

	public function get_user_dialogs( $params_name, $params_value )
	{
		$dialogs = DB::getInstance() -> query("SELECT * FROM us_dialogs");

		$dialogs_count = $dialogs -> count();

		for( $i = 0; $i < $dialogs_count; $i++ ) $users[$i] = explode(";", $dialogs -> results()[$i] -> users);

		$data = array(
					   "dialog_id" => array(),
					   "mes_name" => array(),
					   "mes_avatar" => array(),
					   "mes_date" => array(),
					   "counter" => 0
					  );
		$index = 0;
		$dat_ind = $dialogs_count;
		for( $i = 0; $i < $dialogs_count; $i++ )
		{
			for( $z = 0; $z < count($users[$i]); $z++ )
			if( $users[$i][$z] == $params_value[0] )
			{
				if( $z == 0 )
				{
					$oth_us = DB::getInstance() -> query("SELECT id, main_name, sys_avatar FROM us_main WHERE id = ".$users[$i][1]);
						$data["mes_name"][$index] = $oth_us -> results()[$dat_ind] -> main_name;
						$data["mes_id"][$index] = $oth_us -> results()[$dat_ind] -> id;
						$data["mes_avatar"][$index] = $oth_us -> results()[$dat_ind] -> sys_avatar;
						$dat_ind++;
						$oth_mes = DB::getInstance() -> query("SELECT * FROM us_messages WHERE dialog_id = ".$dialogs -> results()[$i] -> id." ORDER BY id DESC");
						$data["mes_text"][$index] = base64_decode($oth_mes -> results()[$dat_ind] -> text);
						$data["mes_date"][$index] = $oth_mes -> results()[$dat_ind] -> date;
						$dat_ind += $oth_mes -> count();
				}
				else if( $z == 1 )
				{
					$oth_us = DB::getInstance() -> query("SELECT id, main_name, sys_avatar FROM us_main WHERE id = ".$users[$i][0]);
						$data["mes_name"][$index] = $oth_us -> results()[$dat_ind] -> main_name;
						$data["mes_avatar"][$index] = $oth_us -> results()[$dat_ind] -> sys_avatar;
						$data["mes_id"][$index] = $oth_us -> results()[$dat_ind] -> id;
						$dat_ind ++;

						$oth_mes = DB::getInstance() -> query("SELECT * FROM us_messages WHERE dialog_id = ".$dialogs -> results()[$i] -> id." ORDER BY id DESC");
						$data["mes_text"][$index] = base64_decode($oth_mes -> results()[$dat_ind] -> text);
						$data["mes_date"][$index] = $oth_mes -> results()[$dat_ind] -> date;
						$dat_ind += $oth_mes -> count();
				}
				else
				{
						$oth_us = DB::getInstance() -> query("SELECT id, main_name, sys_avatar FROM us_main WHERE id = ".$users[$i][$z]);
						$data["mes_name"][$index] = $oth_us -> results()[$dat_ind] -> main_name;
						$data["mes_avatar"][$index] = $oth_us -> results()[$dat_ind] -> sys_avatar;
						$data["mes_id"][$index] = $oth_us -> results()[$dat_ind] -> id;
						$dat_ind ++;

						$oth_mes = DB::getInstance() -> query("SELECT * FROM us_messages WHERE dialog_id = ".$dialogs -> results()[$i] -> id." ORDER BY id DESC");
						$data["mes_text"][$index] = base64_decode($oth_mes -> results()[$dat_ind] -> text);
						$data["mes_date"][$index] = $oth_mes -> results()[$dat_ind] -> date;
						$dat_ind += $oth_mes -> count();
				}

				$data["dialog_id"][$index] = $dialogs -> results()[$i] -> id;
				$index++;
			}
		}

		$data["counter"] = $index;
		echo json_encode($data);
	}

	public function get_messages( $params_name, $params_value )
	{
		$mes = DB::getInstance() -> query("SELECT * FROM us_messages WHERE dialog_id = ".$params_value[0]);

		$data = array(
					  "id" => array(),	
					  "dialog_id" => $params_value[0],
					  "text" => array(),
					  "date" => array(),
					  "name" => array(),
					  "avatar" => array(),
					  "counter" => 0
					 );

		$mes_counter = $mes -> count();

		$is_user = false;
		for( $i = 0; $i < $mes_counter; $i++ )
		{
			if($mes -> results()[$i] -> from_id == $params_name[0])
			{
				$is_user = true;
			}
		}

		if($is_user == true)
		{
			for( $i = 0; $i < $mes_counter; $i++ )
			{
				$user = DB::getInstance() -> query( "SELECT id, main_name, sys_avatar FROM us_main WHERE id = ".$mes -> results()[$i] -> from_id );
				$data["text"][$i] = nl2br(smile(base64_decode(htmlspecialchars($mes -> results()[$i] -> text))));
				$data["date"][$i] = $mes -> results()[$i] -> date;
				$data['id'][$i] = $user -> results()[$mes_counter+$i] -> id;
				$data["name"][$i] = $user -> results()[$mes_counter+$i] -> main_name;
				$data["avatar"][$i] = $user -> results()[$mes_counter+$i] -> sys_avatar;
			}
			$data["counter"] = $mes_counter;
		}
		return json_encode($data);
	}

	public function gallery_modal( $params_name, $params_value )
	{
		$html = "";
		require_once './include/api/gallery_modal.php';
		return $html;
	}

	public function get_search( $params_name, $params_value )
	{
		$html = "";
		
		require_once './include/api/get_search.php';

		return $html;
	}

	public function get_notice( $params_name, $params_value )
	{
		$data = array();

		require_once './include/api/get_notice.php';

		return json_encode($data);
	}

	public function create_dialog( $params_name, $params_value )
	{
		date_default_timezone_set("Europe/Kiev");

		$us_dialogs_c = DB::getInstance() -> query("SELECT id,users FROM us_dialogs WHERE users LIKE'%".$params_name[0]."%' AND users LIKE'%".$params_value[0]."%'");


		if($us_dialogs_c -> count() == 0)
		{
			DB::getInstance() -> no_returns("INSERT INTO us_dialogs(users)
				VALUES(
					'".$params_name[0].";".$params_value[0]."'
				)");

			$id = DB::getInstance() -> query("SELECT id FROM us_dialogs");

			DB::getInstance() -> no_returns("INSERT INTO us_messages(from_id,dialog_id,text,date)
				VALUES(
					".$params_name[0].",
					".$id -> results()[$id->count()-1] -> id.",
					'".base64_encode(htmlspecialchars(urldecode($params_value[1])))."',
					'".date('Y-m-d')."'
				)");
				return $id -> results()[$id->count()-1] -> id;
		}
		else
		{
			DB::getInstance() -> no_returns("INSERT INTO us_messages(from_id,dialog_id,text,date)
				VALUES(
					".$params_name[0].",
					".$us_dialogs_c -> results()[0] -> id.",
					'".base64_encode(htmlspecialchars(urldecode($params_value[1])))."',
					'".date('Y-m-d')."'
				)");
			return $us_dialogs_c -> results()[0] -> id;
		}

	}

	public function search_audio( $params_name, $params_value )
	{
		$audio = DB::getInstance() -> query("SELECT * FROM us_audio WHERE title LIKE '%".$params_value[0]."%'");
		$html = "";

		$html.="<ul>";
		for($i = 0 ; $i < $audio -> count(); $i ++){
			$html.="<li>";
				$html.="<div>";
					$html.="<p>".$audio -> results()[$i] -> title."</p>";
				$html.="</div>";
				$html.="<a href='".$audio -> results()[$i] -> url."' class='sc-player'>".$audio -> results()[$i] -> title."</a>";
			$html.="</li>";
		}
		$html.="</ul>";

		return $html;
	}

	//-------------------------NOTICE

	public function delete_dialog_notice( $params_name, $params_value )
	{
		DB::getInstance() -> no_returns("UPDATE us_messages SET is_read=1 WHERE dialog_id = ".$params_value[0]." AND from_id != ".$params_name[0]);
	}
}
?>